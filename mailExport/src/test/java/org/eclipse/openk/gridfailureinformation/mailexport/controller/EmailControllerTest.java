/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.mailexport.controller;

import org.eclipse.openk.gridfailureinformation.mailexport.MailExportApplication;
import org.eclipse.openk.gridfailureinformation.mailexport.service.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.mail.MessagingException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = MailExportApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class EmailControllerTest {

    @MockBean
    private EmailService emailService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldReturnOkonSend() throws Exception {
        mockMvc.perform(post("/mail")
                .contentType(MediaType.APPLICATION_JSON)
                .content("test@test.de"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldReturnStatus() throws Exception {
        doThrow(new MessagingException()).when(emailService).sendTestMail(any());
        mockMvc.perform(post("/mail")
                .contentType(MediaType.APPLICATION_JSON)
                .content("test@test.de")
        ).andExpect(status().is5xxServerError());
    }

    @Test
    public void shouldReturnUnauthorized() throws Exception {
        mockMvc.perform(post("/mail")
                .contentType(MediaType.APPLICATION_JSON)
                .content("test@test.de"))
                .andExpect(status().isUnauthorized());
    }



}