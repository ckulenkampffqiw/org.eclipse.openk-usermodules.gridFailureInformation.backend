/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.mapper;

import org.eclipse.openk.gridfailureinformation.samointerface.constants.Constants;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.SAMOOutage;
import org.eclipse.openk.gridfailureinformation.samointerface.dtos.StoerungsauskunftUserNotification;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SAMOMapper {

    ForeignFailureDataDto toForeignFailureDataDto(SAMOOutage srcEntity);

}