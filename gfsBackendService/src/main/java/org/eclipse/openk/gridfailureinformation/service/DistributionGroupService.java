/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.mapper.DistributionGroupMapper;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroup;
import org.eclipse.openk.gridfailureinformation.model.TblDistributionGroupMember;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupMemberRepository;
import org.eclipse.openk.gridfailureinformation.repository.DistributionGroupRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DistributionGroupService {

    @Autowired
    private DistributionGroupRepository distributionGroupRepository;

    @Autowired
    private DistributionGroupMapper distributionGroupMapper;

    @Autowired
    private DistributionGroupMemberRepository distributionGroupMemberRepository;


    public List<DistributionGroupDto> getDistributionGroups() {

        return distributionGroupRepository.findAll().stream()
                .map( distributionGroupMapper::toDistributionGroupDto )
                .collect(Collectors.toList());
    }

    public DistributionGroupDto getDistributionGroupByUuid(UUID uuid ) {
        TblDistributionGroup tblDistributionGroup = distributionGroupRepository.findByUuid(uuid)
                .orElseThrow(NotFoundException::new);
        return distributionGroupMapper.toDistributionGroupDto(tblDistributionGroup);
    }

    @Transactional
    public DistributionGroupDto insertDistributionGroup(DistributionGroupDto distributionGroupDto) {
        TblDistributionGroup distributionGroupToSave = distributionGroupMapper.toTblDistributionGroup(distributionGroupDto);
        distributionGroupToSave.setUuid(UUID.randomUUID());

        TblDistributionGroup savedDistributionGroup = distributionGroupRepository.save(distributionGroupToSave);
        return distributionGroupMapper.toDistributionGroupDto(savedDistributionGroup);
    }

    @Transactional
    public void deleteDistributionGroup(UUID uuid) {
        TblDistributionGroup existingDistributionGroup = distributionGroupRepository.findByUuid(uuid)
                .orElseThrow( () -> new BadRequestException("distribution.group.uuid.not.existing"));

        List<TblDistributionGroupMember> tblDistributionGroupMemberList = distributionGroupMemberRepository.findByTblDistributionGroupUuid(uuid);
        for (TblDistributionGroupMember member : tblDistributionGroupMemberList) {
            distributionGroupMemberRepository.delete(member);
        }
        distributionGroupRepository.delete(existingDistributionGroup);
    }

    @Transactional
    public DistributionGroupDto updateGroup(DistributionGroupDto distributionGroupDto){
        TblDistributionGroup group = distributionGroupRepository.findByUuid(distributionGroupDto.getUuid())
                .orElseThrow(() -> new NotFoundException("distribution.group.uuid.not.existing"));

        TblDistributionGroup groupToSave = distributionGroupMapper.toTblDistributionGroup(distributionGroupDto);
        groupToSave.setId(group.getId());

        TblDistributionGroup savedGroup = distributionGroupRepository.save(groupToSave);
        return distributionGroupMapper.toDistributionGroupDto(savedGroup);
    }

}
