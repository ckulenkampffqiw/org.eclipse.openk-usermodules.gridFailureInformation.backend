/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.api;


import org.eclipse.openk.gridfailureinformation.viewmodel.FESettingsDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "${services.sitCache.name}")
public interface SitCacheApi {

    @PostMapping("/internal-sit")
    public void postPublicFailureInfos(
            @RequestBody List<FailureInformationDto> failureInfoToPublish);

    @PostMapping("/fe-settings")
    public void postFeSettings(
            @RequestBody FESettingsDto feSettings);

}
