/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Data
@Entity
@Table( name = "tbl_failinfo_distgroup")
public class TblFailureInformationDistributionGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,  generator = "TBL_FAILINFO_DISTGROUP_ID_SEQ")
    @SequenceGenerator(name = "TBL_FAILINFO_DISTGROUP_ID_SEQ", sequenceName = "TBL_FAILINFO_DISTGROUP_ID_SEQ", allocationSize = 1)
    @Column(name = "id", updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn( name = "fk_tbl_failure_information")
    private TblFailureInformation failureInformation;

    @ManyToOne
    @JoinColumn( name = "fk_tbl_distribution_group")
    private TblDistributionGroup distributionGroup;
}
