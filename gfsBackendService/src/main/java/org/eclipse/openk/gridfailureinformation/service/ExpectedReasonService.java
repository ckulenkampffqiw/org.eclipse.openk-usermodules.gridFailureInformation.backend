/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.mapper.ExpectedReasonMapper;
import org.eclipse.openk.gridfailureinformation.repository.BranchRepository;
import org.eclipse.openk.gridfailureinformation.repository.ExpectedReasonRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.ExpectedReasonDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExpectedReasonService {

    @Autowired
    private ExpectedReasonRepository expectedReasonRepository;

    @Autowired
    private ExpectedReasonMapper expectedReasonMapper;

    @Autowired
    BranchRepository branchRepository;

    public List<ExpectedReasonDto> getExpectedReasons(Optional<String> branchOpt) {
        if (branchOpt.isPresent()) {
            return expectedReasonRepository.findByBranch(branchOpt.get()).stream()
                    .map(expectedReasonMapper::toExpectedReasonDto)
                    .collect(Collectors.toList());
        } else {
            return expectedReasonRepository.findAll().stream()
                    .map( expectedReasonMapper::toExpectedReasonDto )
                    .collect(Collectors.toList());
        }
    }

}
