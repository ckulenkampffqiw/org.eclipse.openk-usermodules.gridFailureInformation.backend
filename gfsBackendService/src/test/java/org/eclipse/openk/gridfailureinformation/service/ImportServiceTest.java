/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.bpmn.impl.GfiProcessState;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.util.ImportDataValidator;
import org.eclipse.openk.gridfailureinformation.viewmodel.BranchDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.ForeignFailureDataDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.ImportDataDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.RadiusDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StatusDto;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.powermock.reflect.Whitebox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})

public class ImportServiceTest {
    @Qualifier("myJobManagerService")
    @Autowired
    private ImportService importService;

    @Autowired
    private ImportDataValidator importDataValidator;

    @MockBean
    private RadiusService radiusService;

    @MockBean
    private BranchService branchService;

    @MockBean
    private StatusService statusService;

    @MockBean
    private FailureInformationService failureInformationService;

    @MockBean
    private ProcessHelper processHelper;

    @Test
    void shouldImportDifferentRadiiProperly() throws Exception {
        List<RadiusDto> radiusDtos = getRadiusDtoList();

        FailureInformationDto failureInformationDto = MockDataHelper.mockFailureInformationDto();

        when(radiusService.getRadii()).thenReturn(radiusDtos);
        Whitebox.setInternalState(importService, "radiusService", radiusService); // don't know why this is needed suddenly

        Whitebox.invokeMethod(importService, "importRadius", 10L, failureInformationDto);
        assertEquals(radiusDtos.get(0).getUuid(), failureInformationDto.getRadiusId());

        Whitebox.invokeMethod(importService, "importRadius", 11L, failureInformationDto);
        assertEquals(radiusDtos.get(0).getUuid(), failureInformationDto.getRadiusId());

        Whitebox.invokeMethod(importService, "importRadius", 45L, failureInformationDto);
        assertEquals(radiusDtos.get(0).getUuid(), failureInformationDto.getRadiusId());

        Whitebox.invokeMethod(importService, "importRadius", 46L, failureInformationDto);
        assertEquals(radiusDtos.get(1).getUuid(), failureInformationDto.getRadiusId());

        Whitebox.invokeMethod(importService, "importRadius", 100L, failureInformationDto);
        assertEquals(radiusDtos.get(1).getUuid(), failureInformationDto.getRadiusId());

        Whitebox.invokeMethod(importService, "importRadius", 1000L, failureInformationDto);
        assertEquals(radiusDtos.get(2).getUuid(), failureInformationDto.getRadiusId());

        Whitebox.invokeMethod(importService, "importRadius", 1001L, failureInformationDto);
        assertEquals(radiusDtos.get(2).getUuid(), failureInformationDto.getRadiusId());


        when(radiusService.getRadii()).thenReturn(new ArrayList<>());
        assertThrows(InternalServerErrorException.class, () ->
                Whitebox.invokeMethod(importService, "importRadius", 1000L, failureInformationDto)
        );
    }

    private List<RadiusDto> getRadiusDtoList() {
        List<RadiusDto> radiusDtos = new ArrayList<>(3);
        radiusDtos.add(MockDataHelper.mockRadiusDto());
        radiusDtos.add(MockDataHelper.mockRadiusDto());
        radiusDtos.add(MockDataHelper.mockRadiusDto());

        radiusDtos.get(0).setRadius(10L);
        radiusDtos.get(1).setRadius(100L);
        radiusDtos.get(2).setRadius(1000L);
        return radiusDtos;
    }

    @Test
    public void shouldNotImportMessagesBecauseImportDataDtoInvalid() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMetaId("§§§"); // invalid char

        assertFalse(importService.validateAndImport(dto));
    }


    @Test
    public void shouldNotImportMessagesBecauseImportDataPayloadNoJson() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("Fizlipuzli");

        assertFalse(importService.validateAndImport(dto));
    }

    @Test
    public void shouldNotImportMessagesBecauseImportDataPayloadNotValid() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"xxxx\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": true,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\"\n" +
                "  }");

        assertFalse(importService.validateAndImport(dto));
    }

    @Test
    public void shouldImportMessagesCorrectlyWithPlanned() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": true,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);

        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        importService.validateAndImport(dto);

        verify(failureInformationService, times(1))
                .insertFailureInfo(any( FailureInformationDto.class), eq(GfiProcessState.PLANNED));
    }


    @Test
    public void shouldImportMessagesCorrectlyWithNew() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);

        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        importService.validateAndImport(dto);

        verify(failureInformationService, times(1))
                .insertFailureInfo(any( FailureInformationDto.class), eq(GfiProcessState.NEW));
    }


    @Test
    public void shouldImportUpdateMessageLeavingStatus() throws Exception {
        ImportDataDto importDataDto = MockDataHelper.mockImportDataDto();
        importDataDto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"InDaHood\",\n" +
                "    \"failureBegin\": \"2019-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 44,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": \"ND\",\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": \"Haferbox\",\n" +
                "    \"stationId\": \"34500\",\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);
        when( statusService.getStatusFromId( anyLong()) ).thenReturn(MockDataHelper.mockStatusDto("neu", UUID.randomUUID()));
        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        FailureInformationDto dtoFromDB = MockDataHelper.mockFailureInformationDto();
        UUID condensedId = UUID.randomUUID();
        dtoFromDB.setFailureInformationCondensedId(condensedId);
        dtoFromDB.setCondensedCount(77L);
        dtoFromDB.setCondensed(true);
        dtoFromDB.setObjectReferenceExternalSystem("666777888");

        when( failureInformationService.findByObjectReferenceExternalSystem(anyString())).thenReturn(dtoFromDB);
        when(failureInformationService.updateFailureInfo(any(FailureInformationDto.class)))
                .then((Answer<FailureInformationDto>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (FailureInformationDto) args[0];
                });


        ForeignFailureDataDto dtoFromBus = importDataValidator.readSafeForeignFailureInfo(importDataDto);
        FailureInformationDto dtoResult = Whitebox.invokeMethod(importService, "doImport", importDataDto, dtoFromBus);

        verify(failureInformationService, times(1))
                .updateFailureInfo(any( FailureInformationDto.class));

        assertEquals(branchStromDto.getUuid(), dtoResult.getBranchId());
        assertEquals("Belfast", dtoResult.getCity());
        assertEquals("Schlimmer Fehler im System", dtoResult.getDescription());
        assertEquals("InDaHood", dtoResult.getDistrict());
        Calendar cal = Calendar.getInstance();
        cal.setTime(dtoResult.getFailureBegin());
        assertEquals( 2019, cal.get(Calendar.YEAR));
        assertEquals( "10b", dtoResult.getHousenumber());
        assertTrue( BigDecimal.valueOf(12.345d).equals(dtoResult.getLatitude()) );
        assertTrue( BigDecimal.valueOf(44).equals(dtoResult.getLongitude()) );
        assertTrue( dtoFromDB.getStatusInternId().equals(dtoResult.getStatusInternId()));
        assertEquals( "3456", dtoResult.getPostcode());
        assertEquals( "ND", dtoResult.getPressureLevel());
        assertEquals( "Haferbox", dtoResult.getStationDescription());
        assertEquals( "34500", dtoResult.getStationId());
        assertEquals( "Oxfordstreet", dtoResult.getStreet());
        assertEquals( "HS", dtoResult.getVoltageLevel());
        assertTrue( condensedId.equals(dtoResult.getFailureInformationCondensedId()));
        assertEquals( 77L, dtoResult.getCondensedCount() );
        assertTrue( dtoResult.getCondensed());
        assertEquals( importDataDto.getAssembledRefId(), dtoResult.getObjectReferenceExternalSystem());
//        assertTrue( dtoFromDB.getStatusExternId().equals(dtoResult.getStatusExternId()));
        assertTrue( dtoFromDB.getFailureClassificationId().equals(dtoResult.getFailureClassificationId()));
    }


    @Test
    public void shouldImportUpdateMessageSettingStatusUpdated() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"voltageLevel\": \"HS\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);
        StatusDto qualifiedStatusDto = MockDataHelper.mockStatusDto("qualified", UUID.randomUUID());
        StatusDto canceledStatusDto = MockDataHelper.mockStatusDto("canceled", UUID.randomUUID());
        StatusDto completedStatusDto = MockDataHelper.mockStatusDto("completed", UUID.randomUUID());
        StatusDto udpatedStatusDto = MockDataHelper.mockStatusDto("updated", UUID.randomUUID());

        when( statusService.getStatusFromId( GfiProcessState.QUALIFIED.getStatusValue()) ).thenReturn(qualifiedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.COMPLETED.getStatusValue()) ).thenReturn(completedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.CANCELED.getStatusValue()) ).thenReturn(canceledStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.UPDATED.getStatusValue()) ).thenReturn(udpatedStatusDto);
        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        FailureInformationDto existingDto = MockDataHelper.mockFailureInformationDto();
        existingDto.setStatusInternId(qualifiedStatusDto.getUuid());
        when( failureInformationService.findByObjectReferenceExternalSystem(anyString())).thenReturn(existingDto);

        importService.validateAndImport(dto);

         verify(failureInformationService, times(1))
                .updateFailureInfo(any( FailureInformationDto.class));
    }

    @Test
    public void shouldNotImportUpdateMessageOnComplete() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);
        StatusDto qualifiedStatusDto = MockDataHelper.mockStatusDto("qualified", UUID.randomUUID());
        StatusDto canceledStatusDto = MockDataHelper.mockStatusDto("canceled", UUID.randomUUID());
        StatusDto completedStatusDto = MockDataHelper.mockStatusDto("completed", UUID.randomUUID());
        StatusDto udpatedStatusDto = MockDataHelper.mockStatusDto("updated", UUID.randomUUID());

        when( statusService.getStatusFromId( GfiProcessState.QUALIFIED.getStatusValue()) ).thenReturn(qualifiedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.COMPLETED.getStatusValue()) ).thenReturn(completedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.CANCELED.getStatusValue()) ).thenReturn(canceledStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.UPDATED.getStatusValue()) ).thenReturn(udpatedStatusDto);
        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        FailureInformationDto existingDto = MockDataHelper.mockFailureInformationDto();
        existingDto.setStatusInternId(completedStatusDto.getUuid());
        when( failureInformationService.findByObjectReferenceExternalSystem(anyString())).thenReturn(existingDto);

        importService.validateAndImport(dto);

        verify(failureInformationService, times(0))
                .updateFailureInfo(any( FailureInformationDto.class));
    }

    @Test
    public void shouldNotImportUpdateMessageOnCanceled() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);
        StatusDto qualifiedStatusDto = MockDataHelper.mockStatusDto("qualified", UUID.randomUUID());
        StatusDto canceledStatusDto = MockDataHelper.mockStatusDto("canceled", UUID.randomUUID());
        StatusDto completedStatusDto = MockDataHelper.mockStatusDto("completed", UUID.randomUUID());
        StatusDto udpatedStatusDto = MockDataHelper.mockStatusDto("updated", UUID.randomUUID());

        when( statusService.getStatusFromId( GfiProcessState.QUALIFIED.getStatusValue()) ).thenReturn(qualifiedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.COMPLETED.getStatusValue()) ).thenReturn(completedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.CANCELED.getStatusValue()) ).thenReturn(canceledStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.UPDATED.getStatusValue()) ).thenReturn(udpatedStatusDto);
        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        FailureInformationDto existingDto = MockDataHelper.mockFailureInformationDto();
        existingDto.setStatusInternId(canceledStatusDto.getUuid());
        when( failureInformationService.findByObjectReferenceExternalSystem(anyString())).thenReturn(existingDto);

        importService.validateAndImport(dto);

        verify(failureInformationService, times(0))
                .updateFailureInfo(any( FailureInformationDto.class));
    }

    @Test
    public void shouldImportMessagesCorrectlyWithNewAutopublish() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\",\n" +
                "    \"autopublish\": \"true\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);

        FailureInformationDto savedFailureInformationDto = MockDataHelper.mockFailureInformationDto();
        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());
        when( failureInformationService.insertFailureInfo(any(), any())).thenReturn(savedFailureInformationDto);
        //when( failureInformationService.insertPublicationChannelForFailureInfo(any(), anyString(), anyBoolean())).thenReturn(new FailureInformationPublicationChannelDto());

        importService.validateAndImport(dto);

        verify(failureInformationService, times(1))
                .insertFailureInfo(any( FailureInformationDto.class), eq(GfiProcessState.QUALIFIED));
    }

    @Test
    public void shouldImportUpdateMessageAutopublish() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\",\n" +
                "    \"autopublish\": \"true\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);
        StatusDto qualifiedStatusDto = MockDataHelper.mockStatusDto("qualified", UUID.randomUUID());
        StatusDto canceledStatusDto = MockDataHelper.mockStatusDto("canceled", UUID.randomUUID());
        StatusDto completedStatusDto = MockDataHelper.mockStatusDto("completed", UUID.randomUUID());
        StatusDto udpatedStatusDto = MockDataHelper.mockStatusDto("updated", UUID.randomUUID());

        when( statusService.getStatusFromId( GfiProcessState.QUALIFIED.getStatusValue()) ).thenReturn(qualifiedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.COMPLETED.getStatusValue()) ).thenReturn(completedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.CANCELED.getStatusValue()) ).thenReturn(canceledStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.UPDATED.getStatusValue()) ).thenReturn(udpatedStatusDto);
        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        FailureInformationDto existingDto = MockDataHelper.mockFailureInformationDto();
        existingDto.setStatusInternId(qualifiedStatusDto.getUuid());
        when( failureInformationService.findByObjectReferenceExternalSystem(anyString())).thenReturn(existingDto);

        importService.validateAndImport(dto);

        verify(failureInformationService, times(1))
                .updateFailureInfo(any( FailureInformationDto.class));
    }

    @Test
    public void shouldImportUpdateMessageOnceOnly() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\",\n" +
                "    \"onceOnlyImport\": \"true\",\n" +
                "    \"autopublish\": \"false\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);
        StatusDto qualifiedStatusDto = MockDataHelper.mockStatusDto("qualified", UUID.randomUUID());
        StatusDto canceledStatusDto = MockDataHelper.mockStatusDto("canceled", UUID.randomUUID());
        StatusDto completedStatusDto = MockDataHelper.mockStatusDto("completed", UUID.randomUUID());
        StatusDto udpatedStatusDto = MockDataHelper.mockStatusDto("updated", UUID.randomUUID());

        when( statusService.getStatusFromId( GfiProcessState.QUALIFIED.getStatusValue()) ).thenReturn(qualifiedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.COMPLETED.getStatusValue()) ).thenReturn(completedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.CANCELED.getStatusValue()) ).thenReturn(canceledStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.UPDATED.getStatusValue()) ).thenReturn(udpatedStatusDto);
        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        FailureInformationDto existingDto = MockDataHelper.mockFailureInformationDto();
        existingDto.setStatusInternId(qualifiedStatusDto.getUuid());
        when( failureInformationService.findByObjectReferenceExternalSystem(anyString())).thenReturn(existingDto);

        importService.validateAndImport(dto);

        verify(failureInformationService, times(0))
                .updateFailureInfo(any( FailureInformationDto.class));
    }

    @Test
    public void shouldImportUpdateMessageExcludeEquals() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\",\n" +
                "    \"excludeEquals\": \"true\",\n" +
                "    \"excludeAlreadyEdited\": \"true\",\n" +
                "    \"autopublish\": \"false\"\n" +
                "  }");

        ForeignFailureDataDto foreignFailureDataDto = importDataValidator.readSafeForeignFailureInfo(dto);

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);
        StatusDto qualifiedStatusDto = MockDataHelper.mockStatusDto("qualified", UUID.randomUUID());
        StatusDto canceledStatusDto = MockDataHelper.mockStatusDto("canceled", UUID.randomUUID());
        StatusDto completedStatusDto = MockDataHelper.mockStatusDto("completed", UUID.randomUUID());
        StatusDto udpatedStatusDto = MockDataHelper.mockStatusDto("updated", UUID.randomUUID());

        when( statusService.getStatusFromId( GfiProcessState.QUALIFIED.getStatusValue()) ).thenReturn(qualifiedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.COMPLETED.getStatusValue()) ).thenReturn(completedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.CANCELED.getStatusValue()) ).thenReturn(canceledStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.UPDATED.getStatusValue()) ).thenReturn(udpatedStatusDto);
        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        FailureInformationDto existingDto = importService.setNewFromForeignDto(dto, foreignFailureDataDto);
        existingDto.setStatusInternId(qualifiedStatusDto.getUuid());
        when( failureInformationService.findByObjectReferenceExternalSystem(anyString())).thenReturn(existingDto);

        importService.validateAndImport(dto);

        verify(failureInformationService, times(0))
                .updateFailureInfo(any( FailureInformationDto.class));
    }

    @Test
    public void shouldImportUpdateMessageExcludeAlreadyEdited() {
        ImportDataDto dto = MockDataHelper.mockImportDataDto();
        dto.setMessageContent("{\n" +
                "    \"branch\": \"S\",\n" + // invalid branch
                "    \"city\": \"Belfast\",\n" +
                "    \"description\": \"Schlimmer Fehler im System\",\n" +
                "    \"district\": \"\",\n" +
                "    \"failureBegin\": \"2020-11-19T14:13:15.666Z\",\n" +
                "    \"housenumber\": \"10b\",\n" +
                "    \"latitude\": 12.345,\n" +
                "    \"longitude\": 0,\n" +
                "    \"planned\": false,\n" +
                "    \"postcode\": \"3456\",\n" +
                "    \"pressureLevel\": null,\n" +
                "    \"radiusInMeters\": 678,\n" +
                "    \"stationDescription\": null,\n" +
                "    \"stationId\": null,\n" +
                "    \"street\": \"Oxfordstreet\",\n" +
                "    \"voltageLevel\": \"HS\",\n" +
                "    \"excludeEquals\": \"true\",\n" +
                "    \"excludeAlreadyEdited\": \"true\",\n" +
                "    \"autopublish\": \"false\"\n" +
                "  }");

        BranchDto branchStromDto = MockDataHelper.mockBranchDto();
        branchStromDto.setName("S");
        when( branchService.findByName(eq("S"))).thenReturn(branchStromDto);
        StatusDto qualifiedStatusDto = MockDataHelper.mockStatusDto("qualified", UUID.randomUUID());
        StatusDto canceledStatusDto = MockDataHelper.mockStatusDto("canceled", UUID.randomUUID());
        StatusDto completedStatusDto = MockDataHelper.mockStatusDto("completed", UUID.randomUUID());
        StatusDto udpatedStatusDto = MockDataHelper.mockStatusDto("updated", UUID.randomUUID());

        when( statusService.getStatusFromId( GfiProcessState.QUALIFIED.getStatusValue()) ).thenReturn(qualifiedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.COMPLETED.getStatusValue()) ).thenReturn(completedStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.CANCELED.getStatusValue()) ).thenReturn(canceledStatusDto);
        when( statusService.getStatusFromId( GfiProcessState.UPDATED.getStatusValue()) ).thenReturn(udpatedStatusDto);
        when( radiusService.getRadii() ).thenReturn(getRadiusDtoList());

        FailureInformationDto existingDto = MockDataHelper.mockFailureInformationDto();
        existingDto.setStatusInternId(qualifiedStatusDto.getUuid());
        when( failureInformationService.findByObjectReferenceExternalSystem(anyString())).thenReturn(existingDto);

        importService.validateAndImport(dto);

        verify(failureInformationService, times(0))
                .updateFailureInfo(any( FailureInformationDto.class));
        verify(processHelper, times(0)).resetPublishedStateForChannels(any());
    }

}
