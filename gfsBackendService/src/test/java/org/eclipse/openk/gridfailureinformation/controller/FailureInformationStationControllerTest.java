/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.openk.gridfailureinformation.GridFailureInformationApplication;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationStationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationStationDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.StationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = GridFailureInformationApplication.class)
@AutoConfigureMockMvc()
@ActiveProfiles("test")
public class FailureInformationStationControllerTest {

    @MockBean
    private FailureInformationStationService failureInformationStationService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnStationsForFailureInfo() throws Exception {
        List<StationDto> stationDtoList = MockDataHelper.mockStationDtoList();
        when(failureInformationStationService.findStationsByFailureInfo(any(UUID.class))).thenReturn(stationDtoList);

        mockMvc.perform(get("/grid-failure-informations/" + UUID.randomUUID().toString() + "/stations"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldAssignStationToFailureInfo() throws Exception {
        FailureInformationStationDto failureInfoStationDto = MockDataHelper.mockFailureInformationStationDto();

        when( failureInformationStationService.insertFailureInfoStation(any(UUID.class), any(StationDto.class))).thenReturn(failureInfoStationDto);

        mockMvc.perform(post("/grid-failure-informations/" + UUID.randomUUID().toString() + "/stations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(failureInfoStationDto)))
                .andExpect(jsonPath("$.failureInformationId", is(failureInfoStationDto.getFailureInformationId().intValue())))
                .andExpect(jsonPath("$.stationStationId", is(failureInfoStationDto.getStationStationId())))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldDeleteStationForFailureInformation() throws Exception {
        mockMvc.perform(delete("/grid-failure-informations/" + UUID.randomUUID().toString() + "/stations/" + UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }
}