/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.model.TblStation;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
public class DecideFailureInfoUpdatedTest {

    @Qualifier("myFailureInformationService")
    @Autowired
    private FailureInformationService failureInformationService;

    @MockBean
    private ProcessHelper processHelper;
    @MockBean
    private StatusRepository statusRepository;
    @MockBean
    private FailureInformationRepository failureInformationRepository;
    @MockBean
    private StationRepository stationRepository;
    @MockBean
    private AddressRepository addressRepository;

    @Test
    public void shouldCall_DecideFailureInfoUpdated_Result_Updated() {
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        refStatus.setId(GfiProcessState.QUALIFIED.getStatusValue());
        refStatus.setStatus("qualified");

        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        UUID statusUpdated = UUID.randomUUID();
        fiDto.setStatusInternId(statusUpdated);

        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        fiTbl.setId(777L);
        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();

        when(statusRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(refStatus));
        when(processHelper.getProcessStateFromStatusUuid(any( UUID.class ))).thenReturn(GfiProcessState.UPDATED);
        when(statusRepository.findByUuid(any( UUID.class ))).thenReturn(Optional.of(refStatus));
        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);
        when( processHelper.storeFailureFromViewModel(any(FailureInformationDto.class)))
                .then((Answer<FailureInformationDto>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (FailureInformationDto) args[0];
                });

        FailureInformationDto savedDto = failureInformationService.updateFailureInfo(fiDto);

        assertEquals(fiDto.getUuid(), savedDto.getUuid());
        assertEquals(statusUpdated, fiDto.getStatusInternId());
    }

    @Test
    public void shouldCall_DecideFailureInfoUpdated_Result_Completed() {
        RefStatus refStatus = MockDataHelper.mockRefStatusCreated();
        refStatus.setId(GfiProcessState.QUALIFIED.getStatusValue());
        refStatus.setStatus("qualified");

        FailureInformationDto fiDto = MockDataHelper.mockFailureInformationDto();
        UUID statusCompleted = UUID.randomUUID();
        fiDto.setStatusInternId(statusCompleted);

        TblFailureInformation fiTbl = MockDataHelper.mockTblFailureInformation();
        fiTbl.setId(777L);
        TblStation tblStation = MockDataHelper.mockTblStation();
        List<TblAddress> addressList = MockDataHelper.mockTblAddressList();

        when(processHelper.getProcessStateFromStatusUuid(any( UUID.class ))).thenReturn(GfiProcessState.COMPLETED);
        when(statusRepository.findByUuid(any( UUID.class ))).thenReturn(Optional.of(refStatus));
        when(statusRepository.findById(anyLong())).thenReturn(Optional.of(refStatus));
        when(failureInformationRepository.findByUuid(any(UUID.class))).thenReturn(Optional.of(fiTbl));
        when(stationRepository.findByStationId(anyString())).thenReturn(Optional.of(tblStation));
        when(addressRepository.findByStationId(anyString())).thenReturn(addressList);
        when( processHelper.storeFailureFromViewModel(any(FailureInformationDto.class)))
                .then((Answer<FailureInformationDto>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (FailureInformationDto) args[0];
                });

        FailureInformationDto savedDto = failureInformationService.updateFailureInfo(fiDto);

        assertEquals(fiDto.getUuid(), savedDto.getUuid());
        assertEquals(statusCompleted, fiDto.getStatusInternId());
    }

}
