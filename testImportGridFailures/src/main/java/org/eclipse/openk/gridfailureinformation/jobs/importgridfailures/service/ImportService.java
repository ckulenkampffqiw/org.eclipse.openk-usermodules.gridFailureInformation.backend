/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.exceptions.InternalServerErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.messaging.MessageChannel;

@Service
@Log4j2
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "rabbitmq")
public class ImportService {

    @Autowired
    private MessageChannel failureImportChannel;

    @Autowired
    private ObjectMapper objectMapper;

    public void  pushForeignFailure(ForeignFailureMessageDto foreignFailureMessageDto) {

        try {
                failureImportChannel.send(
                        MessageBuilder.withPayload(
                                objectMapper.writeValueAsString(foreignFailureMessageDto.getPayload()))
                        .setHeader("metaId", foreignFailureMessageDto.getMetaId())
                        .setHeader("description", foreignFailureMessageDto.getDescription())
                        .setHeader("source", foreignFailureMessageDto.getSource())
                        .build());

        } catch (Exception e) {
            log.debug(e.getMessage(), e);
            throw new InternalServerErrorException("could.not.push.message");
        }

    }

}
