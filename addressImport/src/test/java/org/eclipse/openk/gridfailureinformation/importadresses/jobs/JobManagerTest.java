/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.jobs;

import org.eclipse.openk.gridfailureinformation.importadresses.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.importadresses.service.AddressImportService;
import org.junit.jupiter.api.Test;
import org.mockito.internal.stubbing.answers.DoesNothing;
import org.powermock.reflect.Whitebox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;

import javax.xml.ws.http.HTTPException;

import static java.lang.Boolean.TRUE;
import static java.lang.Boolean.FALSE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
public class JobManagerTest {

    @Autowired
    JobManager jobManager;

    @MockBean
    AddressImportService addressImportService;

    @Test
    public void shouldTriggerStartImport() {
        jobManager.triggerStartImport();
        Boolean startImport = Whitebox.getInternalState(jobManager, "startImport");
        assertEquals(TRUE, startImport);
    }

    @Test
    public void shouldImportOnTriggerAndSetStartImportBackToFalse() throws Exception {
        Whitebox.setInternalState(jobManager, "startImport", TRUE);

        try {
            Whitebox.invokeMethod(jobManager, "importOnTrigger");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Boolean startImport = Whitebox.getInternalState(jobManager, "startImport");
        assertEquals(FALSE, startImport);
    }

    @Test
    public void shouldImportOnTriggerAndDontChangeTheValueFromStartImport() {
        Whitebox.setInternalState(jobManager, "startImport", FALSE);

        try {
            Whitebox.invokeMethod(jobManager, "importOnTrigger");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Boolean startImport = Whitebox.getInternalState(jobManager, "startImport");
        assertEquals(FALSE, startImport);
    }

    @Test
    public void shouldImportAddressdataAndInvokeImportAddresses() {
        jobManager.cleanUp = false;
        try {
            Whitebox.invokeMethod(jobManager, "importAddressdata");
        } catch (Exception e) {
            e.printStackTrace();
        }
        verify(addressImportService, times(1)).importAddresses(false);
    }


    @Test
    public void shouldNotImportAddressdataDueToAnException() {
        jobManager.cleanUp = false;

        doThrow(new HTTPException(HttpStatus.PROCESSING.value())).when(addressImportService).importAddresses(anyBoolean());
        try {
            Whitebox.invokeMethod(jobManager, "importAddressdata");
        } catch (Exception e) {
            e.printStackTrace();
        }
        verify(addressImportService, times(1)).importAddresses(false);
    }


    @Test
    public void shouldNotImportAddressdataDueToAnUnknownException() throws Exception {
        jobManager.cleanUp = false;

        doThrow(new HTTPException(HttpStatus.INTERNAL_SERVER_ERROR.value())).when(addressImportService).importAddresses(anyBoolean());

        assertThrows(HTTPException.class, () -> Whitebox.invokeMethod(jobManager, "importAddressdata") );

    }
}
