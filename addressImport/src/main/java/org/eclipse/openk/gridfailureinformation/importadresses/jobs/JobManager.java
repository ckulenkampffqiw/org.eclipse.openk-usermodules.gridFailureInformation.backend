/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.jobs;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.importadresses.service.AddressImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.xml.ws.http.HTTPException;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

@EnableScheduling
@Component
@Log4j2
public class JobManager {

    @Value("${adressimport.cleanup}")
    public boolean cleanUp;

    @Autowired
    AddressImportService addressImportService;
    private final Object lock = new Object();

    private Boolean startImport = FALSE;

    public void triggerStartImport() {
        synchronized (lock) {
            startImport = TRUE;
        }
    }

    @Scheduled(cron="0/3 0/1 * 1/1 * *") // every 3 seconds
    private void importOnTrigger() {
        boolean go = false;

        synchronized (lock) {
            if( startImport.equals(TRUE) ) {
                go = true;
                startImport = FALSE;
            }
        }

        if( go ) {
            importAddressdata();
        }

    }

    @Scheduled(cron= "${adressimport.cron}")
    private void importAddressdata() {
        try {
            addressImportService.importAddresses(cleanUp);
        }
        catch( HTTPException e ) {
            if( e.getStatusCode() != HttpStatus.PROCESSING.value() ) {
                throw( e );
            }
            else {
                log.info("Jobmanager triggered import, but import already in work");
            }
        }
    }
}
