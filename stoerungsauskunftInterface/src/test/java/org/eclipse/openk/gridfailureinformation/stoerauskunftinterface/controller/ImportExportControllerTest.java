/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.With;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.StoerungsauskunftInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api.StoerungsauskunftApi;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service.ImportExportService;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.support.MockDataHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = StoerungsauskunftInterfaceApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ImportExportControllerTest {

    @MockBean
    private StoerungsauskunftApi stoerungsauskunftApi;

    @MockBean
    private ImportExportService importExportService;

    @Autowired
    private MockMvc mockMvc;


    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldCallImport() throws Exception {

        mockMvc.perform(get("/stoerungsauskunft/usernotification-import-test"))
                .andExpect(status().is2xxSuccessful());

        verify(importExportService, times(1)).importUserNotifications();
    }

    @Test
    public void shouldCallImportAndReturnUnauthorized() throws Exception {

        mockMvc.perform(get("/stoerungsauskunft/usernotification-import-test"))
                .andExpect(status().isUnauthorized());

        verify(importExportService, times(0)).importUserNotifications();
    }

    @Test
    @WithMockUser(value = "mockedUser")
    public void shouldCallExport() throws Exception {

        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        mockMvc.perform(post("/stoerungsauskunft/outage-export-test")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(rabbitMqMessageDto)))
                .andExpect(status().is2xxSuccessful());

        verify(importExportService, times(1)).exportStoerungsauskunftOutage(rabbitMqMessageDto);
    }

    @Test
    public void shouldCallExportAndReturnUnauthorized() throws Exception {

        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        mockMvc.perform(post("/stoerungsauskunft/outage-export-test")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(rabbitMqMessageDto)))
                .andExpect(status().isUnauthorized());

        verify(importExportService, times(0)).exportStoerungsauskunftOutage(rabbitMqMessageDto);
    }
}